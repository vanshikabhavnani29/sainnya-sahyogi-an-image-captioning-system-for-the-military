import config
import text
from text import load_or_build_vocabulary
import tensorflow
from tensorflow.train import Checkpoint, CheckpointManager
import model
from model import build_model
# image captioning
def get_image_encoder(cnn):
    """Create feature extraction layer for the cnn.
    """
    # load model
    image_model = tf.keras.models.load_model("/content/drive/MyDrive/encoder_model.h5")
    new_input = image_model.input
    hidden_layer = image_model.layers[-2].output
    pretrained_image_model = Model(new_input, hidden_layer)
    return pretrained_image_model


def image_preprocessing_function(cnn):
    """Returns a function that can load and preprocess images for a specific cnn model.

    Args:
        cnn {string} -- Name of the CNN used to encode image features

    Returns:
        function -- Function to load and prepare images for the given CNN
    """
    image_size = IMAGE_SIZE[cnn]
    from tensorflow.keras.applications.inception_resnet_v2 import preprocess_input

    def load_and_preprocess_image(image_file):
        image = tf.io.read_file(image_file)
        image = tf.image.decode_jpeg(image, channels=3)
        image = tf.image.resize(image, image_size)
        image = preprocess_input(image)
        return image, image_file

    return load_and_preprocess_image


def generate_captions_with_beam_search(model, img_features, vocabulary, beam_width=3, normalize_by_length=True):
    encoder = model.encoder
    decoder = model.decoder
    tokenizer = model.tokenizer
    start_token = vocabulary.start
    end_token = vocabulary.end

    # get batch size
    batch_size = img_features.shape[0]
    # Initialization of hidden states
    batch_hidden = decoder.reset_state(batch_size=1)
    # Passes visual features through encoder
    batch_features = encoder(img_features)
    predicted_sequences = []
    for idx in tqdm(range(1), desc='beam'):
        # Initialize the hypothesis: start_token will be the initial input
        # Replicate the initial states K times for the first step.
        hyps = [Hypothesis([tf.convert_to_tensor(start_token)], 0.0, batch_hidden[idx])] * beam_width
        features = tf.stack([batch_features[idx]] * beam_width)
        # Run beam search
        results = []
        steps = 0
        while steps < config.max_length and len(results) < beam_width:
            latest_tokens = [h.latest_token for h in hyps]
            states = [h.state for h in hyps]

            # Last tokens become next decoder input, a tensor with shape (beam_size, 1)
            dec_input = tf.expand_dims(latest_tokens, 1)
            # Convert array of hidden states to tensor with shape (beam_size, rnn_units)
            hidden = tf.convert_to_tensor(states)
            # Pass input, image features and hidden state to get new predictions (output) and hidden state.
            # Predictions is tensor with shape (beam_size, vocabulary_size)
            predictions, hidden, _ = decoder(dec_input, features, hidden)
            topk_log_probs, topk_ids = tf.nn.top_k(predictions, k=beam_width * 2)

            # topk_ids = topk_ids.numpy()

            # Extend each hypothesis.
            all_hyps = []
            # The first step takes the best K results from first hyps. Following
            # steps take the best K results from K*K hyps.
            num_beam_source = 1 if steps == 0 else len(hyps)
            for i in range(num_beam_source):
                hyp, hid = hyps[i], hidden[i]
                for j in range(beam_width * 2):
                    all_hyps.append(hyp.extend(topk_ids[i, j], topk_log_probs[i, j], hid))

            # Filter and collect any hypotheses that have the end token.
            hyps = []
            for h in best_hypothesis(all_hyps, normalize_by_length):
                if h.latest_token == end_token:
                    # Pull the hypothesis off the beam if the end token is reached.
                    results.append(h)
                else:
                    # Otherwise continue to extend the hypothesis.
                    hyps.append(h)
                if len(hyps) == beam_width or len(results) == beam_width:
                    break
            steps += 1

        if steps == config.max_length:
            results.extend(hyps)

        best_hyp = best_hypothesis(results, normalize_by_length)[0]
        predicted_sequences.append([t.numpy() for t in best_hyp.tokens])
        # predicted_sequences.append(best_hyp.tokens)

    return predicted_sequences


def eval(model, img_features, vocabulary, config):
    optimizer = tf.optimizers.get(config.optimizer)
    ckpt_manager, ckpt = get_checkpoint_manager(model, optimizer, config.checkpoints_dir, config.max_checkpoints)
    status = ckpt.restore(ckpt_manager.latest_checkpoint)
    if ckpt_manager.latest_checkpoint:
        status.assert_existing_objects_matched()
    epoch = int(ckpt_manager.latest_checkpoint.split('-')[-1])
    results = []
    predicted_captions = generate_captions_with_beam_search(
        model, img_features, vocabulary,
        config.beam_width, config.normalize_by_length)
    results.append(predicted_captions)
    return results




def get_checkpoint_manager(model, optimizer, checkpoints_dir, max_checkpoints=None):
    """Obtains a checkpoint manager to manage model saving and restoring.

    Arguments:
        model (mode.ImageCaptionModel): object containing encoder, decoder and tokenizer
        optimizer (tf.optimizers.Optimizer): the optimizer used during the backpropagation step
        config (config.Config): Values for various configuration options

    Returns:
        tf.train.CheckpointManager, tf.train.Ckeckpoint
    """

    ckpt = Checkpoint(encoder=model.encoder,
                      decoder=model.decoder,
                      optimizer=optimizer)
    ckpt_manager = CheckpointManager(ckpt, checkpoints_dir, max_to_keep=max_checkpoints)

    return ckpt_manager, ckpt


#main calling function

def run_IC(filename):
    # filename='1275832390.jpg'
    config = Config()

    cnn = config.cnn
    IMAGE_SIZE = {'inception_resnet_v2': (299, 299)}
    image_size = IMAGE_SIZE[cnn]



    # step 1 take image src

    # vocabulary building
    vocabulary = load_or_build_vocabulary(config)
    # step 2 fetaure extraction of image
    encoder = get_image_encoder(cnn)
    filepath='/content/'+filename
    image_dataset = Dataset.from_tensor_slices(sorted([filepath,]))
    image_dataset = image_dataset.map(image_preprocessing_function(cnn), num_parallel_calls=tf.data.experimental.AUTOTUNE).batch(1)
    for images, image_filenames in tqdm(image_dataset, desc='image_batch'):
            # obtain batch of image features
            image_features = encoder(images)
            image_features = tf.reshape(image_features, (image_features.shape[0], -1, image_features.shape[3]))

    # step 3 building whole model
    model = build_model(config, vocabulary)
    sequence = eval(model, image_features, vocabulary, config)
    #sequence=list(sequence)
    sequence=sequence[0]
    print(sequence)
    for k, sequence in enumerate(sequence):
        pred_cap = vocabulary.seq2text(sequence)

    # file wirte
    # f = open('filename.txt', 'w')
    # f.truncate()
    # f.write(pred_cap)
    print(pred_cap)
    return pred_cap
