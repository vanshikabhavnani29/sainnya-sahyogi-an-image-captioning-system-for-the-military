import os
# from tensorflow.keras.models import load_model
# import pandas as pd
# import numpy as np
# import matplotlib.pyplot as plt
# from keras.utils import to_categorical
# from numpy import array
from flask import *
from werkzeug.utils import secure_filename
# import cv2
# import functions
# from functions import run_IC

app= Flask(__name__)
UPLOAD_FOLDER = 'static/uploads/'
app.secret_key = "secret key"
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['MAX_CONTENT_LENGTH'] = 16 * 1024 * 1024

ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg', 'gif'])



def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


@app.route('/')
def message():
    return render_template("index.html")

@app.route('/', methods=['POST'])
def upload_image():
    if 'file' not in request.files:
        flash('No file part')
        return redirect(request.url)
    file = request.files['file']
    if file.filename == '':
        flash('No image selected for uploading')
        return redirect(request.url)
    if file and allowed_file(file.filename):
        filename = secure_filename(file.filename)
        file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
		cv2.imwrite(os.path.join(app.config['UPLOAD_FOLDER'], filename), file)
        # img=image_process(filename)
        # test_model(img)
        run_model(os.path.join(app.config['UPLOAD_FOLDER'], filename))
        # f=open("filename.txt", "r")
        # content = f.read()
        return render_template('index.html', filename=os.path.join(app.config['UPLOAD_FOLDER'], filename),text=content)
    else:
        flash('Allowed image types are -> png, jpg, jpeg, gif')
        return redirect(request.url)
def run_model(filename):
	print(filename)








if __name__ == '__main__':
    app.run(debug=True)

